# Projet assembleur : DIR récursif
L'objectif de ce projet était de coder en C puis en MASM32 un programme
similaire à
la fonction MS-DOS `dir /s`  
[Vous trouverez ici le rapport détaillé de ce projet](RAPPORT/rapport.pdf)

## SIMPLE_DIR
Un simple listing de fichier(s) non récursif.  
`USAGE: dir.exe [directory]`  
Si aucun répertoire n'est précisé agit sur le répertoire courant, en clair
`dir.exe ./*` est strictement similaire à `dir.exe`.

## RECURSIVE_DIR
Un listing de fichiers récursif se comportant identiquement à `dir /s`.  
`USAGE: dir.exe [directory]`  
Si aucun répertoire n'est précisé agit sur le répertoire courant, en clair
`dir.exe .` est strictement similaire à `dir.exe`.
