#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Windows.h>

void printFile(WIN32_FIND_DATA struct_data){
	char* dir=struct_data.dwFileAttributes==FILE_ATTRIBUTE_DIRECTORY?"<DIR>":"     ";
	SYSTEMTIME creation_time;
	FileTimeToSystemTime(&(struct_data.ftCreationTime), &creation_time);
	printf("%02d/%02d/%4d  %02d:%02d    %s  %s\n",   creation_time.wDay,
							   creation_time.wMonth,
							   creation_time.wYear,
							   creation_time.wHour,
							   creation_time.wMinute,
							   dir,
							   struct_data.cFileName);
	return;
}
int dir(char* str){
	char directory_absolute_path[MAX_PATH];
	HANDLE handle;
	WIN32_FIND_DATA struct_data;
	
	handle = FindFirstFile(str, &struct_data);
	GetFullPathName(struct_data.cFileName, MAX_PATH, directory_absolute_path, NULL);
	printf("\n Repertoire %s\n\n", directory_absolute_path);
	do{
		printFile(struct_data);
	}while(FindNextFile(handle, &struct_data));
	FindClose(handle);
	
	return 0;
}
