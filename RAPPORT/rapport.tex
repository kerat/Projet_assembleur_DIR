\documentclass[a4paper,12pt,french,twocolumns]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{babel}
\usepackage{lmodern}
\usepackage[babel=true]{csquotes}
\usepackage{listings}
\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}
\input{listing.tex}

\title{Projet assembleur\\ DIR recursif}
\author{Tarek Marcé}
\date{Mai 2018}

\begin{document}
\maketitle
\tableofcontents
\section*{Introduction}
Ce rapport présente la démarche qui fut la mienne lors de la réalisation d'un programme MASM32 d'affichage récursif du contenu d'un répetoire sur système Windows. Nous détaillerons d'abord brievement les principaux appels système utilisés ainsi que les structures de données qui entrent en jeu. Nous expliquerons ensuite l'algorithme que j'ai développé, puis nous étudierons son implémentation en C. Enfin nous verrons comment cet algorithme à été porté en MASM32.\\
L'ensemble du projet est disponible à cette adresse : \url{https://gitlab.com/kerat/Projet_assembleur_DIR}
\section{Etude de l'API Windows}
Voyons premièrement les fonctions et les structures de données permettant de manipuler l'arborescence des fichiers  mise à disposition des développeurs par le système Windows.
\subsection{Structure WIN32\_FIND\_DATA}
La structure WIN32\_FIND\_DATA est la structure qui porte les méta-données d'un fichier\footnote{Fichier est ici utilisé au sens large du terme, la structure pouvant porter les informations d'un répertoire, d'un lecteur ou autre chose. C'est ce sens général qu'il faudra comprendre lors de l'utilisation du terme fichier dans la suite du document également.}. Elle est décrite comme suit dans la documentation Microsoft :
\begin{lstlisting}[language=C]
typedef struct _WIN32_FIND_DATA {
  DWORD    dwFileAttributes;
  FILETIME ftCreationTime;
  FILETIME ftLastAccessTime;
  FILETIME ftLastWriteTime;
  DWORD    nFileSizeHigh;
  DWORD    nFileSizeLow;
  DWORD    dwReserved0;
  DWORD    dwReserved1;
  TCHAR    cFileName[MAX_PATH];
  TCHAR    cAlternateFileName[14];
} WIN32_FIND_DATA, *PWIN32_FIND_DATA, *LPWIN32_FIND_DATA;
\end{lstlisting}
On retiendra plusieurs champs intéressants que nous utiliserons dans notre programme :
\begin{description}
    \item [dwFileAttributes] Le champ dwFileAttributes est un entier non-signé codé sur 32 bits qui nous permet de savoir à quel type de fichier on a affaire (répertoire, disque, fichier caché, fichier chiffré, etc. ), chaque bit à 1 indique un attribut du fichier. L'attribut qui nous intéressera sera l'attribut répertoire défini par la constante FILE\_ATTRIBUTE\_DIRECTORY=16. Pour vérifier si un fichier est un répertoire il nous faudra donc effectuer l'opération logique dwFileAttributes | FILE\_ATTRIBUTE\_DIRECTORY et observer le résultat. Si le résultat est 16 (ou simplement supérieur à 0) alors le fichier est répertoire, si il est égal à 0 alors le fichier n'est pas un répertoire.
    \item [ftCreationTime] Le champ ftCreationTime est tout simplement la date de création du fichier, sous la forme d'une structure FILETIME. La description de cette structure se trouve à la prochaine section : \ref{subsec:struct_time}
    \item [cFileName] Le champ cFileName contient le nom du fichier sous la forme d'une chaine de caractère C classique (tableau de char) de taille MAX\_PATH=260 (constante système).
\end{description}
\subsection{Structure FILETIME et SYSTEMTIME}
\label{subsec:struct_time}
    La structure FILETIME est une structure contenant deux entiers non signés de 32 bits formant une valeur unique codée sur 64 bits. Cette valeur représente le temps écoulé depuis le 1\ier{} janvier 1970.
    Ce format est délicat à manipuler car peu intuitif pour un être humain.
    C'est pour cela que, dans notre programme nous appellerons la procédure FileTimeToSystemTime qui à partir d'une structure FILETIME transoportant une date fournie en premier paramètre vas écrire une structure SYSTEMTIME contenant cette même date à l'adresse fournie en second paramètre.
    Cette structure SYSTEMTIME est une structure qui contient une data dans un format lisible par un humain. Voici sa description :
    \begin{lstlisting}[language=C]
typedef struct _SYSTEMTIME {
  WORD wYear;
  WORD wMonth;
  WORD wDayOfWeek;
  WORD wDay;
  WORD wHour;
  WORD wMinute;
  WORD wSecond;
  WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME;
    \end{lstlisting}
    Les champs qui nous intéressent sont:
    \begin{description}
        \item [wYear] Ce champ contient l'année sous forme numérique dans un entier.
        \item [wMonth] Ce champ contient le mois, un entier compris entre 1 et 12 inclus.
        \item [wDay] Ce champ contient le jour, un entier compris entre 1 et 31 inclus.
        \item [wHour] Ce champ contient l'heure dans un entier compris entre 0 et 23 inclus.
        \item [wMinute] Ce champs contient les minutes dans un entier compris entre 0 et 59 inclus.
    \end{description}
\subsection{Fonction FindFirstFile}
    La fonction FindFirstFile est la fonction qui va recherché le fichier correspondant au nom fourni en premier argument et écrire à l'adresse fournie en deuxième argument une structure WIN32\_FIND\_DATA correspondant à ce fichier, puis renvoyer un <<handler>> dont nous nous servirons lors de l'appel à FindNextFile. Voici le prototype de la fonction : 
    \begin{lstlisting}[language=C]
HANDLE WINAPI FindFirstFile(
  _In_  LPCTSTR           lpFileName,
  _Out_ LPWIN32_FIND_DATA lpFindFileData
);
    \end{lstlisting}
    En cas d'erreur ou de fichier introuvable la fonction renvoie dans ces deux cas respectifs les valeurs INVALID\_HANDLE\_VALUE et ERROR\_FILE\_NOT\_FOUND.
\subsection{Fonction FindNextFile}
    La fonction FindNextFile prends en premier argument un HANDLER (renvoyé par la fonction FindFirstFile) et écrit à l'adresse fournie en second argument une structure WIN32\_FIND\_DATA contenant les méta-données du fichier après celui trouvé par FindFirstFile.
    Par <<après>> nous entendons par exemple le deuxième fichier d'un répertoire. L'ordre dépend des système et n'est pas forcément alphabétique.\\
    Lorsqu'il n'y a pas de fichier <<après>> celui décrit par le HANDLER la fonction renvoie 0 et n'écrit rien à l'adresse fournie en second paramètre.\\
    Voici la signature de la fonction :
    \begin{lstlisting}[language=C]
BOOL WINAPI FindNextFile(
  _In_  HANDLE            hFindFile,
  _Out_ LPWIN32_FIND_DATA lpFindFileData
);
    \end{lstlisting}
\section{Etude de l'algorithme}
\subsection{Affichage du répertoire}
    Notre programme va premièrement appeller la fonction de l'API Windows FindFirstFile sur le chemin donné par l'entrée utilisateur, puis nous nous servons du handle retourner par FindFirstFile pour appeller FindNextFile dans une boucle qui ne s'arretera que quand FindNextFill renverra la valeur nulle.
    \paragraph{}
    Il est important de noter que la FunctionFindNextFile ne parcours un répertoire que si je chemin fourni à FindFirstFile se termine par une étoile. C'est pourquoi avant de commencer notre boucle d'affichage nous nous assurons d'ajouter un '\*' si le chemin fourni est celui d'un répertoire.
    A chaque itération nous affichons une ligne contenant des informations sur le fichier courrant : Date de création, heure de création, si c'est un dossier ou non et le nom du fichier enfin.
\subsection{Affichage des sous répertoires}
    Nous parcourons le répertoire courant une nouvelle fois avec un nouvel appel à FindFirstFile, puis une boucle de FindNextFile. A chaque itération nous vérifions que le fichier courant est un répertoire (les répertoires '.' et '..' sont ignorés) si c'est le cas on prépare la chaine de caractères avec laquelle on va rappellé notre fonction (récursion).
    La chaine sera formé ainsi :\\
    \textit{Chemin du repertoire initial} + \textit{'\textbackslash'} + \textit{Fichier courant} + \textit{'\textbackslash*'}  
    Enfin on rappelle notre fonction avec ce chemin reformaté.
\section{Implémentation en C}
Nous avons d'abord réalisé notre programme en C afin de se familiariser avec l'API et mettre notre algorithme au point. Ce programme à été testé à la main, à posteriori il en ressort qu'il aurait été plus efficace de prendre du temps pour réaliser des tests unitaires pertinents pour chaque fonctions (même si cela aurait impliqué la recherche et l'appropriation d'un framework adéquat). Le programme est divisisé en deux fichiers sources, dir.c contient toutes les fonctions nécessaires à notre programme et main.c se contente de récupérer l'argument de la ligne de commande et le transmet à la fonction dir.
\section{Implémentation en MASM32}
L'implémentation en MASM32 est simplement une adaptation en MASM32 de notre programme C. Le plus compliqué a été de de savoir comment appeller une fonction avec les bons arguments (par exemple pousser sur la pile l'adresse d'une chaine de caractère et non le premier caractère, ou alors une valeur codé sur 2 octets seulement alors que la fonction attends une valeur sur 4) mais également savoir quelle fonctions systèmes libéraient la pile de ses arguments avant de retourner.
\section*{Sources et réferences documentaires}
    \begin{description}
        \item [Structure WIN32\_FIND\_DATA] \url{https://msdn.microsoft.com/en-us/library/windows/desktop/aa365740(v=vs.85).aspx}
        \item [Constantes FILE\_ATTRIBUTES] \url{https://msdn.microsoft.com/en-us/library/windows/desktop/gg258117(v=vs.85).aspx}
        \item [Type TCHAR] \url{https://docs.microsoft.com/en-us/office/client-developer/outlook/mapi/tchar}
        \item [Constante MAX\_PATH] \url{https://msdn.microsoft.com/en-us/library/windows/desktop/aa365247(v=vs.85).aspx#maxpath}
        \item [Structure FILETIME] \url{https://msdn.microsoft.com/en-us/library/windows/desktop/ms724284(v=vs.85).aspx}
        \item [Procédure FileTimeToSystemTime] \url{https://msdn.microsoft.com/en-us/library/windows/desktop/ms724280(v=vs.85).aspx}
        \item [Structure SYSTEMTIME] \url{https://msdn.microsoft.com/en-us/library/windows/desktop/ms724950(v=vs.85).aspx}
        \item [Fonction FindFirstFile] \url{https://msdn.microsoft.com/en-us/library/windows/desktop/aa364418(v=vs.85).aspx}
        \item [Fonction FindNextFile] \url{https://msdn.microsoft.com/en-us/library/windows/desktop/aa364428(v=vs.85).aspx}
    \end{description}
\end{document}
