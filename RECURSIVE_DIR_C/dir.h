#ifndef _DIR_H
#define _DIR_H
void format_path(char*);
void add_star(char*);
void remove_star(char*);
void get_next_path(char*, char*, char*);
char is_dir(int);
void printFile(WIN32_FIND_DATA);
int dir(char*);
#endif