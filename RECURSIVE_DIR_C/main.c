#include <stdlib.h>
#include <stdio.h>
#include "dir.h"

int main(int argc, char** argv){
	char* directory=NULL;
	
	if(argc<2){
		directory=".\\*";
	}
	else{
		directory=argv[1];
	}
	dir(directory);
	
	return 0;
}