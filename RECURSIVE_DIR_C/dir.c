#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Windows.h>

char test_path(char * path){
	int length = strlen(path);
	return path[length-1] == '\\';
}

void format_path(char * path){
	char * last_slash = strrchr(path, '\\');
	*last_slash = '\0';
	return;
}

void add_star(char* path){
	int length = strlen(path);
	if((length < 2) || !(path[length-2] == '\\' && path[length-1] == '*')){
		strcat(path, "\\*");
	}
	return;
}

void remove_star(char * path){
	int length = strlen(path);
	path[length-2] = '\0';
	return;
}

void get_next_path(char * current_path, char * next_dir, char * next_path){
	memcpy(next_path, current_path, MAX_PATH);
	strcat(next_path, "\\");
	strcat(next_path, next_dir);
	strcat(next_path, "\\*");
	return;
}

char is_dir(int file_attribute){
	return (file_attribute & FILE_ATTRIBUTE_DIRECTORY);
}

void printFile(WIN32_FIND_DATA struct_data){
	char* dir=is_dir(struct_data.dwFileAttributes)?"<DIR>":"     ";
	SYSTEMTIME creation_time;
	FileTimeToSystemTime(&(struct_data.ftCreationTime), &creation_time);
	printf("%02d/%02d/%4d  %02d:%02d    %s  %s\n",   creation_time.wDay,
							   creation_time.wMonth,
							   creation_time.wYear,
							   creation_time.wHour,
							   creation_time.wMinute,
							   dir,
							   struct_data.cFileName);
	return;
}
int dir(char* file_name){
	char is_file = 0;
	char next_path[MAX_PATH] = {0};
	char full_path[MAX_PATH] = {0};
	HANDLE handle = {0};
	WIN32_FIND_DATA struct_data  = {0};
	
	if(test_path(file_name)){
		fprintf(stderr, "Input should not ends with \'\\\'\n");
		return 1;
	}
	handle = FindFirstFile(file_name, &struct_data);
	is_file = !is_dir(struct_data.dwFileAttributes);
	GetFullPathName(file_name, MAX_PATH, full_path, NULL);
	if(!is_file){
		add_star(full_path);
		handle = FindFirstFile(full_path, &struct_data);
		remove_star(full_path);
	}
	else{
		format_path(full_path);
	}
	printf("\n Repertoire %s\n\n", full_path);
	do{
		printFile(struct_data);
	}while(FindNextFile(handle, &struct_data));
	
	if(!is_file){
		FindClose(handle);
		add_star(full_path);
		handle = FindFirstFile(full_path, &struct_data);
		remove_star(full_path);
		while(FindNextFile(handle, &struct_data)){
			if(is_dir(struct_data.dwFileAttributes) &&
			   strcmp(struct_data.cFileName,".") &&
			   strcmp(struct_data.cFileName,"..")){
				get_next_path(full_path, struct_data.cFileName, next_path);
				dir(next_path);
			}
		}
	}
	FindClose(handle);
	
	return 0;
}
