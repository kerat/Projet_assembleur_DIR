.386
.model flat,stdcall
option casemap:none

include \masm32\include\masm32rt.inc

include includes\dir.asm

.DATA
msg db "Chemin : ",0
buffer BYTE 260 dup (?)
format_s db "%260s",0
tt db ".\*",0

.CODE
start:
	push offset msg
	call crt_printf
	add esp, 4
	
	push offset buffer
	push offset format_s
	call crt_scanf
	add esp, 8
	
	push offset buffer
	call dir
	add esp, 4
	
	mov eax, 0
	invoke ExitProcess,eax
	
end start