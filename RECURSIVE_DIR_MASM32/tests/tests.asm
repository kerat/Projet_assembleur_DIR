.386
.model flat,stdcall
option casemap:none

include \masm32\include\masm32rt.inc

; Fichiers à tester
include ..\includes\test_path.asm
include ..\includes\format_path.asm
include ..\includes\add_star.asm
include ..\includes\remove_star.asm
include ..\includes\get_next_path.asm
include ..\includes\is_dir.asm
include ..\includes\printFile.asm

.DATA
str_rep db "rep\",0
str_no_rep db "norep",0
str_format_path db "repwithslash\",0
str_add_star_1 db "addstar",0,0,0
str_add_star_2 db "noaddstar\*",0
str_remove_star db "nostar\*",0
str_cur_path db "current_path",0
buffer1 BYTE 247 dup (?)
str_next_dir db "next_path",0
buffer2 BYTE 250 dup (?)
str_next_path db 0
buffer3 BYTE 259 dup (?)

str_test_test_path_yes db "Test_path yes: %d",10,0
str_test_test_path_no db "Test_path no: %d",10,0
str_test_format_path db "Format_path: %s",10,0
str_test_add_star db "Add_star: %s",10,0
str_test_remove_star db "Remove_star: %s",10,0
str_test_get_next_path db "Get_next_path: %s",10,0
str_test_is_dir_yes db "Is_dir yes: %d",10,0
str_test_is_dir_no db "Is_dir no: %d",10,0
strCommand db "Pause",13,10,0

.CODE
start:
	; Test test_path 1
	push offset str_rep
	call test_path
	add esp, 4
	; Affichage test
	push eax
	push offset str_test_test_path_yes
	call crt_printf
	add esp, 8
	
	; Test test_path 2
	push offset str_no_rep
	call test_path
	add esp, 4
	; Affichage test
	push eax
	push offset str_test_test_path_no
	call crt_printf
	add esp, 8
	
	; Test format_path
	push offset str_format_path
	call format_path
	add esp, 4
	; Affichage test
	push offset str_format_path
	push offset str_test_format_path
	call crt_printf
	add esp, 8
	
	; Test add_star 1
	push offset str_add_star_1
	call add_star
	add esp, 4
	; Affichage test
	push offset str_add_star_1
	push offset str_test_add_star
	call crt_printf
	add esp, 8
	
	; Test add_star 2
	push offset str_add_star_2
	call add_star
	add esp, 4
	; Affichage test
	push offset str_add_star_2
	push offset str_test_add_star
	call crt_printf
	add esp, 8
	
	; Test remove_star
	push offset str_remove_star
	call remove_star
	add esp, 4
	; Affichage test
	push offset str_remove_star
	push offset str_test_remove_star
	call crt_printf
	add esp, 8
	
	; Test get_next_path
	push offset str_next_path
	push offset str_next_dir
	push offset str_cur_path
	call get_next_path
	add esp, 12
	; Affichage test
	push offset str_next_path
	push offset str_test_get_next_path
	call crt_printf
	add esp, 8
	
	;Test is_dir 1
	push 16d
	call is_dir
	add esp, 4
	; Affichage test
	push eax
	push offset str_test_is_dir_yes
	call crt_printf
	add esp, 8
	
	;Test is_dir 2
	push 29d
	call is_dir
	add esp, 4
	; Affichage test
	push eax
	push offset str_test_is_dir_yes
	call crt_printf
	add esp, 8
	
	;Test is_dir 3
	push 13d
	call is_dir
	add esp, 4
	; Affichage test
	push eax
	push offset str_test_is_dir_no
	call crt_printf
	add esp, 8
	
	;Test is_dir 3
	push 13d
	call is_dir
	add esp, 4
	; Affichage test
	push eax
	push offset str_test_is_dir_no
	call crt_printf
	add esp, 8
	
	invoke crt_system, offset strCommand
	mov eax, 0
	invoke ExitProcess,eax
	
end start