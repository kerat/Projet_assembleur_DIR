.DATA
star db "\*",0

.CODE
; Rajoute \* à un chemin qui n'en a pas
add_star PROC
	VAR_LENGTH equ 4                    ; Contiendra la taille de la chaine
	
	push ebp                            ; On sauvegarde ebp sur la pile
	mov ebp, esp                        ; On copie la pile dans ebp pour pouvoir lire les arguments
	mov edx, [ebp+8]                    ; On récupère l'adresse de la chaine à traiter dans edx
	
	sub esp, VAR_LENGTH                 ; On alloue de l'espace à VAR_LENGTH sur la pile
	
	; Appelle à strlen
	push edx
	call lstrlen
	mov edx, [ebp+8]
	
	; Stockage du resultat dans VAR_LENGTH
	mov [ebp-VAR_LENGTH], eax
	
	cmp DWORD PTR [ebp-VAR_LENGTH], 2d
	jb retour
	
	; Indice de l'avant dernier caractère (taille -1) dans ecx
	mov ecx, [ebp-VAR_LENGTH]
	sub ecx, 2d
	add edx, ecx
	
	cmp BYTE PTR [edx], '\'
	jne concat
	add edx, 1d
	cmp BYTE PTR [edx], '*'
	je retour
	
	concat:
		push offset star
		push [ebp+8]
		call crt_strcat
		add esp, 8d
	
	retour:
		mov eax, 0
		add esp, VAR_LENGTH
		pop ebp                         ; On remet l'adresse de retour
		                                ; (en haut de la pile) dans ebp
		ret                             ; On retourne
add_star ENDP