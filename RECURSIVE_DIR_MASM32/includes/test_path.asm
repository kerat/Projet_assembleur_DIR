.CODE
; Test si un chemin est bien un répertoire
; (Concretement regarde si le dernier caractère est un '\')
test_path PROC
	VAR_LENGTH equ 4                    ; Contiendra la taille de la chaine
	
	push ebp                            ; On sauvegarde ebp sur la pile
	mov ebp, esp                        ; On copie la pile dans ebp pour pouvoir lire les arguments
	mov edx, [ebp+8]                    ; On récupère l'adresse de la chaine à traiter dans edx
	
	sub esp, VAR_LENGTH                 ; On alloue de l'espace à VAR_LENGTH sur la pile
	
	; Appelle à strlen
	push edx
	call lstrlen
	mov edx, [ebp+8]
	
	; Stockage du resultat dans VAR_LENGTH
	mov [ebp-VAR_LENGTH], eax
	
	; Indice du dernier caractère (taille -1) dans ecx
	mov ecx, [ebp-VAR_LENGTH]
	sub ecx, 1
	
	; Mise à jour de edx avec l'adresse du dernier caractère
	add edx, ecx
	
	; Comparaison du dernier caractère avec '\'
	cmp BYTE PTR [edx], '\'
	je vrai
	
	faux:
		mov eax, 0d
		jmp retour
	vrai:
		mov eax, 1d
	
	retour:
		add esp, VAR_LENGTH
		pop ebp                         ; On remet l'adresse de retour
		                                ; (en haut de la pile) dans ebp
		ret                             ; On retourne
test_path ENDP