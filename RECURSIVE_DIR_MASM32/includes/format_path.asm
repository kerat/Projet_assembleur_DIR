.CODE
; Supprime le slash final de la chaine
format_path PROC
	ARG1 equ 8
	
	push ebp                            ; On sauvegarde ebp sur la pile
	mov ebp, esp                        ; On copie la pile dans ebp pour pouvoir lire les arguments
	mov edx, [ebp+ARG1]                    ; On récupère l'adresse de la chaine à traiter dans edx
		
	; Appel à strrchr
	push DWORD PTR '\\'
	push edx
	call crt_strrchr
	add esp, 8
		
	; On met un 0 à la place du dernier slash
	mov BYTE PTR [eax], 0
	
	retour:
		mov eax, 0
		pop ebp                         ; On remet l'adresse de retour
		                                ; (en haut de la pile) dans ebp
		ret                             ; On retourne
format_path ENDP