.DATA
print_file db "%02d/%02d/%4d  %02d:%02d    %s  %s",10,0
chevron_dir db "<DIR>",0
no_dir db "     ",0
.DATA?
creation_time SYSTEMTIME <>
.CODE
; Affiche les infos d'un fichier sur une ligne
printFile PROC
	push ebp
	mov ebp, esp
	mov ebx, [ebp+8]
	add ebx, 4
		
	push offset creation_time
	push ebx
	call FileTimeToSystemTime
	
	mov ebx, [ebp+8]
	add ebx, 44d
	push ebx
	
	mov ebx, [ebp+8]
	push [ebx]
	call is_dir
	add esp, 4
	cmp eax, 0
	je if_no_dir
	
	if_dir:
		push offset chevron_dir
		jmp do_printf
	if_no_dir:
		push offset no_dir
	do_printf:
		movzx ebx,WORD PTR [creation_time.wMinute]
		movzx ecx,WORD PTR [creation_time.wHour]
		movzx edx,WORD PTR [creation_time.wYear]
		movzx esi,WORD PTR [creation_time.wMonth]
		movzx edi,WORD PTR [creation_time.wDay]
		push ebx
		push ecx
		push edx
		push esi
		push edi
		push offset print_file
		call crt_printf
		add esp, 32d
	
	retour:
		mov eax, 0
		pop ebp
		ret
printFile ENDP