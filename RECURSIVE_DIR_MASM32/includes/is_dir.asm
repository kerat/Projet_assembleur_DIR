.CODE
; Retourne 1 si ARG1 est un attribut de repertoire
is_dir PROC
	push ebp
	mov ebp, esp
	
	mov eax, DWORD PTR [ebp+8]
	and eax, 16d
	
	retour:
		pop ebp
		ret
is_dir ENDP