.CODE
; Supprime le slash final de la chaine
remove_star PROC
	ARG1 equ 8
	
	push ebp                            ; On sauvegarde ebp sur la pile
	mov ebp, esp                        ; On copie la pile dans ebp pour pouvoir lire les arguments
	mov edx, [ebp+ARG1]                 ; On récupère l'adresse de la chaine à traiter dans edx
		
	; Calcule de la taille
	push edx
	call lstrlen
	mov edx, [ebp+8]
	
	sub eax, 2d
	
	add edx, eax
		
	; On met un 0 à la place de l'avant dernier caractere
	mov BYTE PTR [edx], 0
	
	retour:
		mov eax, 0
		pop ebp                         ; On remet l'adresse de retour
		                                ; (en haut de la pile) dans ebp
		ret                             ; On retourne
remove_star ENDP