include test_path.asm
include format_path.asm
include add_star.asm
include remove_star.asm
include get_next_path.asm
include is_dir.asm
include printFile.asm

.DATA
error_str db "Input should not ends with '\'",10,0
str_print_rep db 10," Repertoire %s",10,10,0
str_point db ".",0
str_point_point db "..",0
.DATA?
sdata WIN32_FIND_DATA <>
.CODE
; Algo principal du programme
dir PROC
	FILE_NAME equ 8d
	handle equ 8d
	full_path equ 268d
	str_next_path equ 528d
	initial_attribute equ 532d

	push ebp
	mov ebp, esp
	
	; Allocation des variables locales sur la pile
	sub esp, 532d
	
	; On verifie que le chemin ne fini pas par '\'
	push [ebp+FILE_NAME]
	call test_path
	add esp, 4
	cmp eax, 0
	je chemin_bon
	; Si oui on quitte
	push offset error_str
	push 2d
	call crt_fprintf
	add esp, 8
	jmp retour_err
	
	; On appelle le premier FindFirstFile
	chemin_bon:
	push offset sdata
	push [ebp+FILE_NAME]
	call FindFirstFile
	; Resultat dans handle
	mov [ebp-handle], eax
	; attribut dans initial_attribute
	mov eax, sdata.dwFileAttributes
	mov DWORD PTR [ebp-initial_attribute], eax
	
	; On recupere le chemin complet
	; dans full_path
	push 0
	mov eax, ebp
	sub eax, full_path
	push eax
	push 260d
	push [ebp+FILE_NAME]
	call GetFullPathName
	
	; On test si c'est un repertoire
	push sdata.dwFileAttributes
	call is_dir
	add esp, 4
	cmp eax, 0
	je it_is_file
	; Si c'est un repertoire :
	; On ajoute un '\*' au chemin
	mov eax, ebp
	sub eax, full_path
	push eax
	call add_star
	add esp, 4
	; On appelle FindFirstFile
	push offset sdata
	mov eax, ebp
	sub eax, full_path
	push eax
	call FindFirstFile
	; Resultat dans handle
	mov [ebp-handle], eax
	; On supprime le '\*' du chemin
	mov eax, ebp
	sub eax, full_path
	push eax
	call remove_star
	add esp, 4
	jmp print_rep
	
	; Si c'est un fichier :
	it_is_file:
	mov eax, ebp
	sub eax, full_path
	push eax
	call format_path
	add esp, 4
	
	; On affiche le repertoire dans lequel on est
	print_rep:
	mov eax, ebp
	sub eax, full_path
	push eax
	push offset str_print_rep
	call crt_printf
	add esp, 8
	
	; On affiche le contenu du repertoire:
	boucle_affichage_1:
	; On affiche le fichier dans sdata
	push offset sdata
	call printFile
	add esp, 4
	; On appelle FindNextFile
	push offset sdata
	push [ebp-handle]
	call FindNextFile
	; Si on est au bout on sort de la boucle
	cmp eax, 0
	jne boucle_affichage_1
	
	; Si le chemin est un repertoire...
	mov eax, [ebp-initial_attribute]
	push eax
	call is_dir
	add esp, 4
	cmp eax, 0
	je fin_boucle
	
	; On ferme le handle ouvert:
	push [ebp-handle]
	call FindClose
	; On ajoute une etoile au chemin
	mov eax, ebp
	sub eax, full_path
	push eax
	call add_star
	add esp, 4
	; On appelle FindFirstFile
	push offset sdata
	mov eax, ebp
	sub eax, full_path
	push eax
	call FindFirstFile
	; On retire le '\*'
	mov [ebp-handle], eax
	mov eax, ebp
	sub eax, full_path
	push eax
	call remove_star
	add esp, 4
	
	; On commence la boucle d'appels recursifs à dir
	boucle_affichage_2:
	; On verifie qu'on a bien affaire à un repertoire
	push sdata.dwFileAttributes
	call is_dir
	add esp, 4
	cmp eax, 0
	je fin_boucle
	; Qui n'est ni '.'
	push offset str_point
	push offset sdata.cFileName
	call crt_strcmp
	add esp, 8
	cmp eax, 0
	je fin_boucle
	; ni '..'
	push offset str_point_point
	push offset sdata.cFileName
	call crt_strcmp
	add esp, 8
	cmp eax, 0
	je fin_boucle
	
	; On appelle get_next_path
	mov eax, ebp
	sub eax, str_next_path ; ARG3
	push eax
	push offset sdata.cFileName ; ARG2
	mov eax, ebp
	sub eax, full_path ; ARG1
	push eax
	call get_next_path
	add esp, 12d
	; On appelle dir avec l'argument str_next_path
	mov eax, ebp
	sub eax, str_next_path
	push eax
	call dir
	add esp, 4
	
	fin_boucle:
	; On appelle FindNextFile
	push offset sdata
	push [ebp-handle]
	call FindNextFile
	; Si il n'y plus d'autres fichiers on sort de la boucle
	cmp eax, 0
	jne boucle_affichage_2
	
	fin:
	; On libere handle avec FindClose
	push [ebp-handle]
	call FindClose
	
	; Return 0
	mov eax, 0
	jmp retour
	; Return 1
	retour_err:
	mov eax, 1
	retour:
		; On desalloue la pile
		add esp, 532d
		pop ebp
		ret
dir ENDP