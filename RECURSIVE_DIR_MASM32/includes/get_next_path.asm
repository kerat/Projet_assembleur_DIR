.DATA
slash db "\",0
slash_star db "\*",0

.CODE
; Concat. ARG1(current_path)+'\'+ARG2(next_dir)+'\*' et  l'ecrit dans ARG3(next_path)
get_next_path PROC
	current_path equ 8
	next_dir equ 12
	next_path equ 16
	
	push ebp
	mov ebp, esp
	
	sub esp, 12d
	
	push 260d
	push [ebp+current_path]
	push [ebp+next_path]
	call crt_memcpy
	add esp, 12d
	
	push offset slash
	push [ebp+next_path]
	call crt_strcat
	add esp, 8d
	
	push [ebp+next_dir]
	push [ebp+next_path]
	call crt_strcat
	add esp, 8d
	
	push offset slash_star
	push [ebp+next_path]
	call crt_strcat
	add esp, 8d
	
	retour:
		add esp, 12d
		mov eax, 0
		pop ebp
		ret
get_next_path ENDP